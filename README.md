# Minetest MSVC buildbot #

To support current developers and future developers in making Windows builds using MSVC, I wrote this scripts.

The most annoying part of compiling Minetest on MSVC are the loads of dependencies. Each have a different build system, different paths, outdated project files,....
The init.bat is using git to automatically clone them into the correct folder and to update the project files.

The make script then compiles the dependencies and calls cmake with the correct arguments to generate a valid Minetest Project file and a working minetest.exe

### Requirements: ###

* [https://git-for-windows.github.io/](Git) (make sure git is added to path)
* [https://cmake.org/download/](CMake) (make sure cmake is added to path) at least version 3.9 is required!
* [https://www.visualstudio.com/](Visual Studio 2017) (make sure to install the C++ workload for Desktop)


### Instructions: ###

1. Copy both files into a new empty folder eg. "Minetest" 
2. Open "x86 Native Tools Command Prompt for VS 2017" or the x64 variant of it, navigate to the folder where the files are, and execute the init.bat script.
3. In the prompt execute the make script like "make.bat Installer". If you want a portable version, run "make.bat Portable"

### Notes: ###

* This should also work with VS 2015 and VS 2013, but is not tested
