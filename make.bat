@REM Minetest MSVC make bot
@REM (c) 201,2018 adrido
@REM
@REM Usage:
@REM Open VS developer command prompt, and cd to this file and execute.
@REM    make.bat [Portable|Installer] [config_type]
@REM    [Portable|Installer] can be either "Portable" or "Installer"
@REM    [config_type] can be empty or Release or Debug
@echo off

SET Platform=%VSCMD_ARG_TGT_ARCH%

IF "%Platform%"=="x86" (
    REM OK
) ELSE IF "%Platform%"=="x64" (
    REM OK
) ELSE (
    echo ERROR: Script not called from VS developer command prompt!
    goto :usage_error
)

IF "%1"=="Portable" (
    SET Portable=TRUE
) ELSE IF "%1"=="Installer" (
    SET Portable=FALSE
) ELSE (
    goto :usage_error
)

IF "%2"=="" (
    echo [config_type] not set. Assuming Release
    SET Config=Release
) ELSE IF "%2"=="Release" (
    SET Config=Release
) ELSE IF "%2"=="Debug" (
    SET Config=Debug
) ELSE (
    goto :usage_error
)

SET Portable=FALSE
IF "%Portable%"=="TRUE" (
    SET PortableStr=Portable
) ELSE (
    SET PortableStr=Installer
)

echo Building Minetest %Platform% %Config% %PortableStr%

REM Old project files still call it Win32 instead of x86
SET LegacyPlatform=%Platform%
IF "%Platform%" EQU "x86" SET LegacyPlatform=Win32

REM Irrlicht is very special... :-/
SET IrrlichtPlatform=%LegacyPlatform%
IF "%Platform%" EQU "x64" SET IrrlichtPlatform=Win64

SET BuildFolderPlatform=
IF "%Platform%" EQU "x64" SET BuildFolderPlatform="x64\"

SET root=%~dp0

REM Irrlicht requires an old DirectX SDK instead of using the Windows SDK
REM This setting dissables D3D9
SET CL=/DNO_IRR_COMPILE_WITH_DIRECT3D_9_

REM Build the Irrlicht.dll
msbuild .\Irrlicht\source\Irrlicht\Irrlicht15.0.sln ^
    /p:Configuration="%Config% - Fast FPU"^
    /p:Platform="%Platform%"^
    /m
SET CL=

mkdir .\SQLite\%Platform%\%Config%
pushd .\SQLite\%Platform%\%Config%
REM Compile sqlite as dll+lib with some Optimizations
cl ..\..\sqlite3.c /DSQLITE_API=__declspec(dllexport) ^
    /Ox /Ob2 /Oi /Ot /GL ^
    /link /dll /out:sqlite3.dll
popd

REM Freetype2
msbuild .\freetype2\builds\windows\vc2010\freetype.sln ^
    /p:Configuration="%Config%"^
    /p:Platform="%LegacyPlatform%"^
    /m

REM LuaJit
pushd .\LuaJIT\src\
call msvcbuild.bat
popd

REM zlib
mkdir zlib\build\%Platform%\
pushd zlib\build\%Platform%\
cmake ..\.. -DCMAKE_GENERATOR_PLATFORM=%LegacyPlatform%
::SET CL=/DZLIB_WINAPI
msbuild zlib.vcxproj ^
    /p:Configuration=%Config%^
    /p:Platform="%LegacyPlatform%"^
    /m
::SET CL=
mkdir include
copy zconf.h include\ /Y
copy ..\..\zlib.h include\ /Y
popd
if errorlevel 1 goto :FAIL

REM Curl
SET CL=/I"%root%zlib\"
echo %CL%
SET LINK=/LIBPATH:%root%zlib\build\%Platform%\%Config%\
pushd .\curl\winbuild
nmake /f Makefile.vc mode=dll ENABLE_WINSSL=yes ENABLE_SSPI=yes WITH_ZLIB=dll MACHINE=%Platform% ZLIB_PATH=%root%zlib\build\%Platform%\
popd
if errorlevel 1 goto :FAIL

REM LibOgg
msbuild .\libogg\win32\VS2015\libogg_dynamic.sln ^
    /p:Configuration="%Config%"^
    /p:Platform="%LegacyPlatform%"^
    /m

REM LibVorbis
REM the vc2008 is using a wrong libary input path, this is the correct one
set "LIB=%root%libogg\win32\VS2015\%LegacyPlatform%\%Config%\;%LIB%"
REM use environment variables for INCLUDE and LIB values
set UseEnv=true


msbuild .\libvorbis\win32\VS2008\libvorbis\libvorbis_dynamic.vcxproj ^
    /p:Configuration="%Config%"^
    /p:Platform="%LegacyPlatform%"^
    /m
set "LIB=%root%libvorbis\win32\VS2008\libvorbis\%LegacyPlatform%\%Config%\;%LIB%"
msbuild .\libvorbis\win32\VS2008\libvorbisfile\libvorbisfile_dynamic.vcxproj ^
    /p:Configuration="%Config%"^
    /p:Platform="%LegacyPlatform%"^
    /m
set UseEnv=false


REM OpanAL Soft
mkdir openal-soft\build\%Platform%\
pushd openal-soft\build\%Platform%\

cmake ..\.. -DCMAKE_GENERATOR_PLATFORM=%LegacyPlatform%^
    -DALSOFT_NO_CONFIG_UTIL=TRUE

msbuild OpenAl.sln ^
    /p:Configuration="%Config%"^
    /p:Platform="%LegacyPlatform%"^
    /m

popd

mkdir %root%\Minetest\cmake-build-release\%Platform%-%PortableStr%
pushd %root%\Minetest\cmake-build-release\%Platform%-%PortableStr%
REM mkdir Build-%Platform%-Portable
REM pushd Build-%Platform%-Portable

cmake %root%\Minetest -DCMAKE_GENERATOR_PLATFORM=%LegacyPlatform% ^
    ^
    -DIRRLICHT_SOURCE_DIR=.\Irrlicht\source\^
    -DIRRLICHT_LIBRARY=%root%Irrlicht\lib\%IrrlichtPlatform%-visualstudio\Irrlicht.lib^
    -DIRRLICHT_DLL=%root%Irrlicht\bin\%IrrlichtPlatform%-visualstudio\Irrlicht.dll^
    -DIRRLICHT_INCLUDE_DIR=%root%Irrlicht\include\^
    ^
    -DSQLITE3_LIBRARY=%root%SQLite\%Platform%\%Config%\sqlite3.lib^
    -DSQLITE3_INCLUDE_DIR=%root%SQLite\^
    -DSQLITE3_DLL:FILEPATH=%root%SQLite\%Platform%\%Config%\sqlite3.dll^
    ^
    -DFREETYPE_INCLUDE_DIR_freetype2=%root%freetype2\include\freetype^
    -DFREETYPE_INCLUDE_DIR_ft2build=%root%freetype2\include^
    -DFREETYPE_LIBRARY=%root%freetype2\objs\%LegacyPlatform%\%Config%\freetype.lib^
    -DFREETYPE_DLL:FILEPATH=%root%freetype2\objs\%LegacyPlatform%\%Config%\freetype.dll^
    ^
    -DENABLE_LUAJIT=1^
    -DLUA_INCLUDE_DIR=%root%LuaJIT\src\^
    -DLUA_DLL=%root%LuaJIT\src\lua51.dll^
    -DLUA_LIBRARY=%root%LuaJIT\src\lua51.lib^
    ^
    -DZLIB_INCLUDE_DIR=%root%zlib\build\%Platform%\include^
    -DZLIB_DLL=%root%zlib\build\%Platform%\%Config%\zlib.dll^
    -DZLIB_LIBRARIES=%root%zlib\build\%Platform%\%Config%\zlib.lib^
    ^
    -DENABLE_CURL=TRUE^
    -DCURL_INCLUDE_DIR=%root%curl\builds\libcurl-vc-%Platform%-release-dll-zlib-dll-ipv6-sspi-winssl\include\^
    -DCURL_DLL=%root%curl\builds\libcurl-vc-%Platform%-release-dll-zlib-dll-ipv6-sspi-winssl\bin\libcurl.dll^
    -DCURL_LIBRARY=%root%curl\builds\libcurl-vc-%Platform%-release-dll-zlib-dll-ipv6-sspi-winssl\lib\libcurl.lib^
    ^
    -DENABLE_SOUND=1^
    -DOGG_DLL=%root%libogg\win32\VS2015\%LegacyPlatform%\%Config%\libogg.dll^
    -DOGG_INCLUDE_DIR=%root%libogg\include^
    -DOGG_LIBRARY=%root%libogg\win32\VS2015\%LegacyPlatform%\%Config%\libogg.lib^
    ^
    -DVORBIS_DLL=%root%libvorbis\win32\VS2008\libvorbis\%LegacyPlatform%\%Config%\libvorbis.dll^
    -DVORBIS_INCLUDE_DIR=%root%libvorbis\include^
    -DVORBIS_LIBRARY=%root%libvorbis\win32\VS2008\libvorbis\%LegacyPlatform%\%Config%\libvorbis.lib^
    -DVORBISFILE_LIBRARY=%root%libvorbis\win32\VS2008\libvorbisfile\%LegacyPlatform%\%Config%\libvorbisfile.lib^
    -DVORBISFILE_DLL=%root%libvorbis\win32\VS2008\libvorbisfile\%LegacyPlatform%\%Config%\libvorbisfile.dll^
    ^
    -DOPENAL_DLL=%root%openal-soft\build\%Platform%\%Config%\OpenAL32.dll^
    -DOPENAL_INCLUDE_DIR=%root%openal-soft\include\AL^
    -DOPENAL_LIBRARY=%root%openal-soft\build\%Platform%\%Config%\OpenAL32.lib^
    ^
    -DENABLE_LEVELDB=0^
    -DENABLE_POSTGRESQL=0^
    -DENABLE_REDIS=0^
    ^
    -DENABLE_SYSTEM_JSONCPP=0^
    -DENABLE_SYSTEM_GMP=0^
    -DENABLE_SPATIAL=0^
    -DENABLE_CURSES=0^
    -DENABLE_GETTEXT=0^
    -DRUN_IN_PLACE=%Portable%^
    -DCMAKE_CXX_FLAGS="/DWIN32_NO_ZLIB_WINAPI /DWIN32 /D_WINDOWS /W3 /GR /EHsc"

msbuild .\src\minetest.vcxproj ^
    /p:Configuration=%Config%^
    /p:Platform="%Platform%"^
    /m
if errorlevel 1 goto :FAIL

cpack -C %Config% --config .\CPackConfig.cmake

popd

goto :EOF

:FAIL
echo:
echo *******************************************************
echo *** Build FAILED -- Please check the error messages ***
echo *******************************************************
goto :EOF

:usage_error
echo [ERROR:%~nx0] Error in script usage. The correct usage is:
goto :usage

:usage
echo Syntax:
echo     %~nx0 [Portable^|Installer] [config_type] 
echo where : 
echo     [Portable^|Installer] can be either "Portable" or "Installer"
echo     [config_type]: {empty} ^| Debug ^| Release 
echo:
echo For example:
echo     %~nx0 Portable
echo     %~nx0 Portable Debug
echo     %~nx0 Portable Release
echo     %~nx0 Installer
echo     %~nx0 Installer Debug
echo     %~nx0 Installer Release

:EOF