@REM Minetest MSVC make bot
@REM (c) 2017,2018 adrido
@REM
@REM Usage:
@REM Open VS developer command prompt, and cd to this file and execute.
@REM    init.bat
@echo off


TITLE Download Irrlicht libary (1/11)
git clone --depth 1 https://bitbucket.org/adrido/irrlicht-stable.git Irrlicht
if errorlevel 1 goto :BAD

TITLE Download SQLite3 libary (2/11)
git clone --depth 1 https://bitbucket.org/adrido/sqlite-stable-msvc.git SQLite
if errorlevel 1 goto :BAD

TITLE Download LuaJIT libary (3/11)
git clone --depth 1 https://github.com/LuaJIT/LuaJIT.git LuaJIT
if errorlevel 1 goto :BAD

TITLE Download freetype2 libary (4/11)
git clone --depth 1 http://git.sv.nongnu.org/r/freetype/freetype2.git freetype2
if errorlevel 1 goto :BAD

TITLE Download zlib libary (5/11)
git clone --depth 1 https://github.com/madler/zlib.git zlib
if errorlevel 1 goto :BAD

TITLE Download curl libary (6/11)
git clone --depth 1 https://github.com/curl/curl.git curl
if errorlevel 1 goto :BAD

TITLE Download Download libogg (dependency ^for vorbis) (7/11)
git clone https://git.xiph.org/ogg.git libogg
if errorlevel 1 goto :BAD

TITLE Download vorbis libary (8/11)
git clone https://git.xiph.org/vorbis.git libvorbis
if errorlevel 1 goto :BAD

TITLE Download OpenAl-Soft libary (9/11)
git clone --depth 1 https://github.com/kcat/openal-soft.git OpenAl-Soft
if errorlevel 1 goto :BAD

TITLE Download Minetest (10/11)
git clone --depth 1 https://github.com/minetest/minetest.git Minetest
if errorlevel 1 goto :BAD

TITLE Download Minetest Game libary (11/11)
pushd .\Minetest\games\
git clone --depth 1 https://github.com/minetest/minetest_game.git minetest_game
popd

TITLE Init Curl

pushd .\curl\
call buildconf.bat
if errorlevel 1 goto :BAD
popd

TITLE Init Libarys
REM Upgrade to current Windows SDK and Platformtoolset
devenv .\Irrlicht\source\Irrlicht\Irrlicht15.0.sln /Upgrade
if errorlevel 1 goto :BAD
devenv .\SQLite\SQLite3.sln /Upgrade
if errorlevel 1 goto :BAD
devenv .\freetype2\builds\windows\vc2010\freetype.sln /Upgrade
if errorlevel 1 goto :BAD
devenv .\libogg\win32\VS2015\libogg_dynamic.sln /Upgrade
if errorlevel 1 goto :BAD
REM VS2010 is broken, so we have to upgrade the VS2008
devenv .\libvorbis\win32\VS2008\vorbis_dynamic.sln /Upgrade
if errorlevel 1 goto :BAD

TITLE
goto :EOF

:BAD
echo:
echo *******************************************************
echo *** Build FAILED -- Please check the error messages ***
echo *******************************************************

:EOF